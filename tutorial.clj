(require 'datomic.api)

(def uri "datomic:dev://localhost:4334/seattle")
; #'user/uri


(def db (datomic.api/create-database uri))
; #'user/db

(import datomic.Peer)
; datomic.Peer

(import datomic.Connection)
; datomic.Connection

(import datomic.Util)
; datomic.Util
(datomic.api/get-database-names "datomic:dev://localhost:4334/*" )
; ("seattle")


(def conn (datomic.api/connect uri))
; #'user/conn

(datomic.api/log conn)
; #datomic.log.LogValue{:db datomic.db.Db@796dd628, :olookup #<cache$lookup_cache$reify__1317 datomic.cache$lookup_cache$reify__1317@7a2ab862>, :root-id #uuid "55b040c5-b59e-4e55-99cd-359c26d56c97", :tail #datomic.db.MemLog{:txes []}}

conn
; #<Connection {:db-id "seattle-14f3b701-fc43-4244-b91b-6205e0f1b03f", :index-rev 0, :basis-t 63, :next-t 1000, :unsent-updates-queue 0, :pending-txes 0}>

(def seattle-db (datomic.api/db conn))

(use '[clojure.java.shell :only [sh]])
; nil

(sh "pwd")

(def data (read-string (slurp "../datomic_tutorial/seattle-schema.edn")))
; #'user/data

(def result (datomic.api/q "[:find ?c :where [?c :community/name]]" seattle-db))
(def result (datomic.api/q
              "
              [:find [?n ...]
               :where
               [?c :community/name ?n]
               [?c :community/type :community.type/twitter]]
              "
              seattle-db))

; this is the same as

(def result (datomic.api/q
              "
              [:find [?n ...]
               :where
               [?c :community/name ?n]
               [?c :community/type ?t]
               [?t :db/ident :community.type/twitter]]
              "
              seattle-db))



result
; ["Magnolia Voice" "Columbia Citizens" "Discover SLU" "Fremont Universe" "Maple Leaf Life" "MyWallingford"]


(def result (datomic.api/q
              "
              [:find [?c_name ...]
               :where
               [?c :community/name ?c_name]
               [?c :community/neighborhood ?n]
               [?n :neighborhood/district ?d]
               [?d :district/region :region/ne]]
              "
              seattle-db))


(def result (datomic.api/q
              "
              [:find ?c_name ?r_name
               :where
               [?c :community/name ?c_name]
               [?c :community/neighborhood ?n]
               [?n :neighborhood/district ?d]
               [?d :district/region ?r]
               [?r :db/ident ?r_name]]
              "
              seattle-db))


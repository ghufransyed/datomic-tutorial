
;; This file contains code examples for getting-started.html. They are
;; written in clojure, for use with Datomic's interactive repl. You can
;; start the repl by running 'bin/repl' from the datomic directory.
;; Once the repl is running, you can copy code into it or, if invoke it
;; directory from your editor, based on your configuration.

(ns datomic-tutorial.tutorial
  (:require [datomic.api :refer [q db] :as d])
  (:require [clojure.pprint :refer :all]))

q
d/q
db


;; store database uri
(def uri "datomic:mem://seattle")
uri

;; create database
(d/create-database uri)


;; connect to database
(def conn (d/connect uri))
conn


;; parse schema edn file
(def schema (read-string (slurp "seattle-schema.edn")))
schema
;; create temp_id from :db.part/db partition
(d/tempid :db.part/db -1)


;; display first statement
(first schema)


;; submit schema transaction
(d/transact conn schema)


;; parse seed data edn file
(def seed-data (read-string (slurp "seattle-data0.edn")))
seed-data


;; display first three statements in seed data transaction
(take 3 seed-data)



;; submit seed data transaction
(d/transact conn seed-data)

;; find all communities, return entity ids
(def result1 (d/q '[:find ?e
                    :where
                    [?e :community/name]]
                  (d/db conn)))


;; get first entity id in results and make an entity map
(ffirst result1)
(def entity-map-1 (d/entity (d/db conn) (ffirst result1)))




;; display the entity map's keys (note: entity is LAZY, so evaluating
;; entity-map just returns key of :db/id and the value. However, requesting
:: keys or values on the entity-map will then return MORE information
;; mapping the identity function over the entity-map will do the same
entity-map-1
(map identity entity-map-1)

(keys entity-map-1)
(vals entity-map-1)




;; display the value of the entity's community name
(:community/name entity-map-1)



;; Use a pull expression to get entities' attributes and values.
(def pull-results (d/q '[:find (pull ?e [*]) :where
                         [?e :community/name]] (db conn)))
pull-results


(def pull-results (q '[:find (pull ?c [*]) :where [?c :community/name]] (db conn)))
(pprint (first pull-results))
(pprint pull-results)



;; for each community, display its name
(pprint (map #(:community/name (first %)) pull-results))



;; for each community, get its neighborhood and display
;; both names
pull-results
(map #(juxt :community/name :community/neighborhood) )
(def result2 (d/q '[:find ?name ?nhood
                    :where
                    [?e :community/name ?name]
                    [?e :community/neighborhood ?n]
                    [?n :neighborhood/name]]
                  (d/db conn)))
result2






;; for the first community, get its neighborhood, then for
;; that neighborhood, get all its communities, and
;; print out their names





;; find all communities and specify returning their  names into a collection



;; find all community names and pull their urls





;; find all categories for community named "belltown"






;; find names of all communities that are twitter feeds






;; find names of all communities that are in the NE region








;; find names and regions of all communities









;; find all communities that are twitter feeds and facebook pages
;; using the same query and passing in type as a parameter











;; using first query



;; using second query with pull



;; find all communities that are twitter feeds or facebook pages using
;; one query and a list of individual parameters








;; find all communities that are non-commercial email-lists or commercial
;; web-sites using a list of tuple parameters










;; find all community names coming before "C" in alphabetical order







;; find the community whose names includes the string "Wallingford"





;; find all communities that are websites and that are about
;; food, passing in type and search string as parameters










;; find all names of all communities that are twitter feeds, using rules










;; find names of all communities in NE and SW regions, using rules
;; to avoid repeating logic




















;; find names of all communities that are in any of the northern
;; regions and are social-media, using rules for OR logic






























;; Find all transaction times, sort them in reverse order



;; pull out two most recent transactions, most recent loaded
;; seed data, second most recent loaded schema



;; make query to find all communities


;; find all communities as of schema transaction



;; find all communities as of seed data transaction



;; find all communities since schema transaction



;; find all communities since seed data transaction




;; parse additional seed data file


;; find all communities if new data is loaded



;; find all communities currently in database


;; submit new data transaction


;; find all communities currently in database


;; find all communities since original seed data load transaction




;; make a new partition




;; make a new community



;; get id for a community, use to transact data








;; retract data for a community


;; retract a community entity







;; get transaction report queue, add new community again













